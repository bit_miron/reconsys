package edu.umn.cs.recsys.uu;

import edu.umn.cs.recsys.dao.MOOCUserDAO;
import it.unimi.dsi.fastutil.longs.LongArrayList;
import it.unimi.dsi.fastutil.longs.LongSet;
import org.grouplens.lenskit.basic.AbstractItemScorer;
import org.grouplens.lenskit.data.dao.ItemEventDAO;
import org.grouplens.lenskit.data.dao.UserEventDAO;
import org.grouplens.lenskit.data.event.Rating;
import org.grouplens.lenskit.data.history.History;
import org.grouplens.lenskit.data.history.RatingVectorUserHistorySummarizer;
import org.grouplens.lenskit.data.history.UserHistory;
import org.grouplens.lenskit.vectors.ImmutableSparseVector;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.SparseVector;
import org.grouplens.lenskit.vectors.VectorEntry;
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * User-user item scorer.
 * @author <a href="http://www.grouplens.org">GroupLens Research</a>
 */
public class SimpleUserUserItemScorer extends AbstractItemScorer {
    private final UserEventDAO userDao;
    private final ItemEventDAO itemDao;

    @Inject
    public SimpleUserUserItemScorer(UserEventDAO udao, ItemEventDAO idao) {
        userDao = udao;
        itemDao = idao;
    }

    @Override
    public void score(long user, @Nonnull MutableSparseVector scores) {
        MutableSparseVector userVector = getUserRatingVector(user).mutableCopy();
        userVector.add(-userVector.mean());

        // TODO Score items for this user using user-user collaborative filtering

        CosineVectorSimilarity cosineVectorSimilary = new CosineVectorSimilarity();

        // This is the loop structure to iterate over items to score
        for (VectorEntry e: scores.fast(VectorEntry.State.EITHER)) {

            long itemID = e.getKey();
            LongSet possibleNeighbors = itemDao.getUsersForItem(itemID);
            MutableSparseVector similarityVector = MutableSparseVector.create(possibleNeighbors, -1.0);

            for(long otherUser : possibleNeighbors) {

                if (otherUser != user) {
                    MutableSparseVector otherUserVector = getUserRatingVector(user).mutableCopy();
                    otherUserVector.add(- otherUserVector.mean());

                    similarityVector.set(otherUser, cosineVectorSimilary.similarity(userVector, otherUserVector));
                }
            }

            LongArrayList possibleNeighborsSortedBySim = similarityVector.keysByValue(true);

            possibleNeighborsSortedBySim.removeElements(30, possibleNeighborsSortedBySim.size());


        }
    }

    /**
     * Get a user's rating vector.
     * @param user The user ID.
     * @return The rating vector.
     */
    private SparseVector getUserRatingVector(long user) {
        UserHistory<Rating> history = userDao.getEventsForUser(user, Rating.class);
        if (history == null) {
            history = History.forUser(user);
        }
        return RatingVectorUserHistorySummarizer.makeRatingVector(history);
    }
}
